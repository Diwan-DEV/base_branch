# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Author: Andrius Laukavičius. Copyright: NOD Baltic JSC
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api


class res_partner(models.Model):
    _inherit = 'res.partner'

    @api.onchange('company_type')
    def onchange_company_type(self):
        for partner in self:
            if partner.company_type == 'company':
                partner.is_company = True
            if partner.company_type == 'branch':
                partner.is_branch = True

    is_branch = fields.Boolean('Is a Branch?')
    parent_root_id = fields.Many2one('res.partner', 'Main Partner',
                                     domain=[('is_company', '=', True),
                                             ('is_branch', '=', False)])

    @api.multi
    def name_get(self):
        res = super(res_partner, self).name_get()
        res_dict = dict(res)
        for record in self:
            if record.parent_root_id and record.is_branch:
                res_dict[record.id] = "%s / %s" % (
                    record.parent_root_id.name, res_dict[record.id])
        return res_dict.items()


